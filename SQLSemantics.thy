theory SQLSemantics
imports SQLSyntax Environments
begin

context Table

begin
text_raw {*\DefineSnippet{projectTable}{*}
primrec projectTable :: "'id Select \<Rightarrow> ('id, 'val) Table \<Rightarrow> (('id, 'val) Table) option"
where
"projectTable all t = Some t" |
"projectTable (list al) t = 
(case (projectCols al (getAttrL t) (getRaw t)) of
  None \<Rightarrow> None |
  Some rt \<Rightarrow> Some (table al rt))"
text_raw {*}%EndSnippet*}

fun evalExpRow :: "('id, 'val) Exp \<Rightarrow> 'id AttrL \<Rightarrow> 'val Row \<Rightarrow> 'val option"
where
"evalExpRow (const v) al r = Some v" |
"evalExpRow (lookup n) (a#al) (v#vl) = 
  (if (n = a)
  then (Some v)
  else (evalExpRow (lookup n) al vl))" |
"evalExpRow e al vl = None"

primrec filterSingleRow :: "('id, 'val) Pred \<Rightarrow> 'id AttrL \<Rightarrow> 'val Row \<Rightarrow> bool"
where
"filterSingleRow ptrue al r = True" |
"filterSingleRow (And p1 p2) al r = ((filterSingleRow p1 al r) \<and> (filterSingleRow p2 al r))" |
"filterSingleRow (Not p) al r = (\<not> (filterSingleRow p al r))" |
"filterSingleRow (Eq e1 e2) al r = 
  (case (evalExpRow e1 al r) of
    None \<Rightarrow> False |
    Some v1 \<Rightarrow> (case (evalExpRow e2 al r) of 
      None \<Rightarrow> False |
      Some v2 \<Rightarrow> (v1 = v2)))" |
"filterSingleRow (Gt e1 e2) al r = 
  (case (evalExpRow e1 al r) of
    None \<Rightarrow> False |
    Some v1 \<Rightarrow> (case (evalExpRow e2 al r) of 
      None \<Rightarrow> False |
      Some v2 \<Rightarrow> (gt v1 v2)))" |
"filterSingleRow (Lt e1 e2) al r = 
  (case (evalExpRow e1 al r) of
    None \<Rightarrow> False |
    Some v1 \<Rightarrow> (case (evalExpRow e2 al r) of 
      None \<Rightarrow> False |
      Some v2 \<Rightarrow> (lt v1 v2)))"

primrec filterRows :: "'val RawTable \<Rightarrow> 'id AttrL \<Rightarrow> ('id, 'val) Pred \<Rightarrow> 'val RawTable"
where
"filterRows Nil al p = Nil" |
"filterRows (r#rt) al p = (let rts = filterRows rt al p in 
  (if (filterSingleRow p al r)
  then r#rts
  else rts))"

primrec filterTable :: "('id, 'val) Table \<Rightarrow> ('id, 'val) Pred \<Rightarrow> ('id, 'val) Table"
where
"filterTable (table al rt) p = table al (filterRows rt al p)"

text_raw {*\DefineSnippet{reduce}{*}
primrec reduce :: "('id, 'val) Query \<Rightarrow> ('id, ('id, 'val) Table) Env \<Rightarrow>
   (('id, 'val) Query) option"
where
"reduce (tvalue t) ts = None" |
"reduce (selectFromWhere s tnl p) ts =
  (if (length tnl = 1)
  then (let n = hd tnl in
    (let maybeTable = lookupEnv n ts in
      case maybeTable of
        None \<Rightarrow> None |
        Some t \<Rightarrow> 
        (let filtered = filterTable t p in
          let maybeSelected = projectTable s filtered in
            case maybeSelected of
              None \<Rightarrow> None |
              Some tsel \<Rightarrow> Some (tvalue tsel))))
  else None)" |
"reduce (union q1 q2) ts =
  (if (isValue q1)
  then (if (isValue q2)
        then Some (tvalue 
          (table (getAttrL (getTable q1)) 
            (rawUnion (getRaw (getTable q1)) (getRaw (getTable q2)))))
        else (let q2reduce = (reduce q2 ts) in
        (case q2reduce of
          None \<Rightarrow> None |
          Some q \<Rightarrow> Some (union q1 q))))
  else  (let q1reduce = (reduce q1 ts) in
    (case q1reduce of
    None \<Rightarrow> None |
    Some q \<Rightarrow> Some (union q q2))))" 
(*<*)|
"reduce (intersection q1 q2) ts =
  (if (isValue q1)
  then (if (isValue q2)
        then Some (tvalue 
          (table (getAttrL (getTable q1)) 
            (rawIntersection (getRaw (getTable q1)) (getRaw (getTable q2)))))
        else (let q2reduce = (reduce q2 ts) in
        (case q2reduce of
          None \<Rightarrow> None |
          Some q \<Rightarrow> Some (intersection q1 q))))
  else  (let q1reduce = (reduce q1 ts) in
    (case q1reduce of
    None \<Rightarrow> None |
    Some q \<Rightarrow> Some (intersection q q2))))" | 
"reduce (difference q1 q2) ts =
  (if (isValue q1)
  then (if (isValue q2)
        then Some (tvalue 
          (table (getAttrL (getTable q1)) 
            (rawDifference (getRaw (getTable q1)) (getRaw (getTable q2)))))
        else (let q2reduce = (reduce q2 ts) in
        (case q2reduce of
          None \<Rightarrow> None |
          Some q \<Rightarrow> Some (difference q1 q))))
  else  (let q1reduce = (reduce q1 ts) in
    (case q1reduce of
    None \<Rightarrow> None |
    Some q \<Rightarrow> Some (difference q q2))))"
(*>*)
text_raw {*}%EndSnippet*}

lemma reduce_INV_selectFromWhere:
  assumes "reduce (selectFromWhere s tnl p) TS = Some q'"
  shows "\<exists> t tsel. lookupEnv (hd tnl) TS = Some t 
    \<and> projectTable s (filterTable t p) = Some tsel \<and> q' = tvalue tsel" 
  using assms 
  by (simp, smt option.case_eq_if option.collapse option.distinct(1) option.inject)

lemma reduce_INV_union_valueq1t_valueq2f: 
  assumes "reduce (union q1 q2) TS = Some q'"
  assumes "isValue q1"
  assumes "\<not>isValue q2"
  shows "\<exists> q2'. (reduce q2 TS = Some q2' \<and> q' = union q1 q2')"
  using assms
  by (simp, metis (mono_tags, lifting) option.case_eq_if option.collapse option.distinct(1) option.inject)

lemma reduce_INV_union_valueq1f: 
  assumes "reduce (union q1 q2) TS = Some q'"
  assumes "\<not>isValue q1"
  shows "\<exists> q1'. (reduce q1 TS = Some q1' \<and> q' = union q1' q2)"
  using assms
  by (smt option.case_eq_if option.collapse option.distinct(1) option.inject reduce.simps(3))

lemma reduce_INV_intersection_valueq1t_valueq2f: 
  assumes "reduce (intersection q1 q2) TS = Some q'"
  assumes "isValue q1"
  assumes "\<not>isValue q2"
  shows "\<exists> q2'. (reduce q2 TS = Some q2' \<and> q' = intersection q1 q2')"
  using assms
  by (simp, metis (mono_tags, lifting) option.case_eq_if option.collapse option.distinct(1) option.inject)

lemma reduce_INV_intersection_valueq1f: 
  assumes "reduce (intersection q1 q2) TS = Some q'"
  assumes "\<not>isValue q1"
  shows "\<exists> q1'. (reduce q1 TS = Some q1' \<and> q' = intersection q1' q2)"
  using assms
  by (smt option.case_eq_if option.collapse option.distinct(1) option.inject reduce.simps(4))

lemma reduce_INV_difference_valueq1t_valueq2f: 
  assumes "reduce (difference q1 q2) TS = Some q'"
  assumes "isValue q1"
  assumes "\<not>isValue q2"
  shows "\<exists> q2'. (reduce q2 TS = Some q2' \<and> q' = difference q1 q2')"
  using assms
  by (simp, metis (mono_tags, lifting) option.case_eq_if option.collapse option.distinct(1) option.inject)

lemma reduce_INV_difference_valueq1f: 
  assumes "reduce (difference q1 q2) TS = Some q'"
  assumes "\<not>isValue q1"
  shows "\<exists> q1'. (reduce q1 TS = Some q1' \<and> q' = difference q1' q2)"
  using assms
  by (smt option.case_eq_if option.collapse option.distinct(1) option.inject reduce.simps(5))


end
end