theory PreservationProof
imports ProgressProof 
begin

context TypedTables
begin

declare [[ smt_timeout = 30 ]]

lemma rawUnionPreservesWelltypedRaw:
  assumes wrt1: "welltypedRawtable tt rt1"
  assumes wrt2: "welltypedRawtable tt rt2"
  shows "welltypedRawtable tt (rawUnion rt1 rt2)"
  using assms
proof (induction rt1)
  case Nil
  then show ?case 
    by simp
next
  case (Cons r rtr)
  have wtr: "welltypedRow tt r"
    by (meson Cons.prems(1) filter_eq_ConsD welltypedRawtable_def)
  have wtrtr: "welltypedRawtable tt rtr" 
    by (metis Cons.prems(1) TypedTables.welltypedRawtable_def filter.simps(2) list.inject wtr)
  have wtru: "welltypedRawtable tt (rawUnion rtr rt2)" 
    by (simp add: Cons.IH wrt2 wtrtr)
  show ?case
  proof (cases "r \<in> (set rt2)")
    case True
    hence ru: "rawUnion (r # rtr) rt2 = rawUnion rtr rt2" 
      by (smt Table.rawUnion.simps(3) list.set_cases)
    show ?thesis 
      by (simp add: ru wtru)
  next
    case False
    hence ru: "rawUnion (r # rtr) rt2 = r # (rawUnion rtr rt2)" 
      by (smt Cons.prems(2) Table.rawUnion.simps(1) Table.rawUnion.simps(2) Table.rawUnion.simps(3) TypedTables.welltypedRawtable_def filter_empty_conv list.set_cases wtrtr)
    show ?thesis
      by (metis TypedTables.welltypedRawtable_def filter.simps(2) ru wtr wtru)
  qed
qed

lemma rawIntersectionPreservesWelltypedRaw:
  assumes wrt1: "welltypedRawtable tt rt1"
  assumes wrt2: "welltypedRawtable tt rt2"
  shows "welltypedRawtable tt (rawIntersection rt1 rt2)"
  using assms
proof (induction rt1)
  case Nil
  then show ?case by simp
next
  case (Cons r rtr)
  have wtr: "welltypedRow tt r"
    by (meson Cons.prems(1) filter_eq_ConsD welltypedRawtable_def)
  have wtrtr: "welltypedRawtable tt rtr" 
    by (metis Cons.prems(1) TypedTables.welltypedRawtable_def filter.simps(2) list.inject wtr)
  have wtri: "welltypedRawtable tt (rawIntersection rtr rt2)" 
    using Cons.IH wrt2 wtrtr 
    by blast
  show ?case
  proof (cases "r \<in> (set rt2)")
    case True
    hence ri: "rawIntersection (r # rtr) rt2 = r # (rawIntersection rtr rt2)" 
      by (smt Table.rawIntersection.simps(3) list.set_cases) 
    show ?thesis 
      by (metis Cons.IH TypedTables.welltypedRawtable_def filter.simps(2) ri wrt2 wtr wtrtr)
  next
    case False
    hence ri: "rawIntersection (r # rtr) rt2 = rawIntersection rtr rt2" 
      by (smt Cons.prems(2) Table.rawIntersection.simps(1) Table.rawIntersection.simps(2) Table.rawIntersection.simps(3) TypedTables.welltypedRawtable_def filter_empty_conv list.set_cases wtrtr)
    show ?thesis
      by (simp add: Cons.IH ri wrt2 wtrtr)
  qed
qed


lemma rawDifferencePreservesWelltypedRaw:
  assumes wrt1: "welltypedRawtable tt rt1"
  assumes wrt2: "welltypedRawtable tt rt2"
  shows "welltypedRawtable tt (rawDifference rt1 rt2)"
  using assms
proof (induction rt1)
  case Nil
  then show ?case by simp
next
  case (Cons r rtr)
  have wtr: "welltypedRow tt r"
    by (meson Cons.prems(1) filter_eq_ConsD welltypedRawtable_def)
  have wtrtr: "welltypedRawtable tt rtr" 
    by (metis Cons.prems(1) TypedTables.welltypedRawtable_def filter.simps(2) list.inject wtr)
  have wtrd: "welltypedRawtable tt (rawDifference rtr rt2)" 
    by (simp add: Cons.IH wrt2 wtrtr)
  show ?case
  proof (cases "r \<in> (set rt2)")
    case True
    hence rd: "rawDifference (r # rtr) rt2 = rawDifference rtr rt2" 
      by (smt Table.rawDifference.simps(3) list.set_cases)
    show ?thesis 
      by (simp add: Cons.IH rd wrt2 wtrtr)
  next
    case False
    hence rd: "rawDifference (r # rtr) rt2 = r # (rawDifference rtr rt2)" 
      by (smt Cons.prems(2) Table.rawDifference.simps(1) Table.rawDifference.simps(2) Table.rawDifference.simps(3) TypedTables.welltypedRawtable_def filter_False list.set_cases wtrtr)
    show ?thesis
      by (metis Cons.IH TypedTables.welltypedRawtable_def filter.simps(2) rd wrt2 wtr wtrtr)
  qed
qed

lemma projectTypeAttrLMatchesProjectTableAttrL:
  assumes wrt: "matchingAttrL tt (getAttrL t)"
  assumes "projectTypeAttrL al tt = Some tt'" 
  assumes "projectTable (list al) t = Some t'"
  shows "matchingAttrL tt' (getAttrL t')"
  using assms
proof (induction al arbitrary: t' tt')
  case Nil
  then show ?case 
    by (simp add: matchingAttrL_def, auto)
next
  case (Cons a alr)
  then show ?case
  proof -
    have tpattrl: "getAttrL t' = a # alr" 
      by (metis (mono_tags, lifting) Cons.prems(3) getAttrL.simps option.case_eq_if option.distinct(1) option.inject projectTable.simps(2))
    obtain tt'' where  ptattrlttpp: "projectTypeAttrL alr tt = Some tt''"
      by (metis (no_types, lifting) Cons.prems(2) option.case_eq_if 
          option.collapse projectTypeAttrL.simps(2))
    obtain ct where fct: "findColType a tt = Some ct" 
      using Cons.prems(2) 
      by fastforce
    have ttpp: "tt' = (a, ct) # tt''" 
      using Cons.prems(2) fct ptattrlttpp 
      by auto
    obtain t'' where pttpp: "projectTable (list alr) t = Some t''"
      by (metis (no_types, lifting) Cons.prems(3) option.case_eq_if projectCols.simps(2) projectTable.simps(2))    
    have ga: "getAttrL t'' = alr" 
      by (metis (mono_tags, lifting) getAttrL.simps option.case_eq_if option.distinct(1) option.inject projectTable.simps(2) pttpp)
    have ma: "matchingAttrL tt'' alr" 
      using Cons.IH ga ptattrlttpp pttpp wrt 
      by blast
    with ttpp tpattrl show ?thesis 
      by (simp add: matchingAttrL_def)
  qed
qed

lemma welltypedEmptyProjection:
  shows "welltypedRawtable Nil (projectEmptyCol rt)"
proof (induction rt)
  case Nil
  then show ?case 
     by (simp add: welltypedRawtable_def)
next
  case (Cons r rtr)
  then show ?case 
    by (simp add: welltypedRawtable_def welltypedRow_def)
qed

lemma projectFirstRawPreservesWelltypedRaw:
  assumes "welltypedRawtable tt rt"
  assumes tte: "tt = (a, ct)#ttr"
  shows "welltypedRawtable [(a, ct)] (projectFirstRaw rt)"
  using assms
proof (induction rt)
  case Nil
  then show ?case 
    by (simp add: welltypedRawtable_def)
next
  case (Cons r rtr)
  have wrtr: "welltypedRawtable tt rtr" 
    by (metis Cons.prems(1) TypedTables.welltypedRawtable_def filter.simps(2) filter_eq_ConsD list.inject)
  have wr: "welltypedRow tt r" 
    by (meson Cons.prems(1) filter_eq_ConsD welltypedRawtable_def)
  obtain f fr where rnonempty: "r = f#fr" 
    by (smt TypedTables.welltypedRow_def dropFirstColRaw.elims length_0_conv list.discI list.inject tte wr)
  with wr tte have ftype: "ft f = ct" 
    by (simp add: welltypedRow_def) 
  have IHcons: "welltypedRawtable [(a, ct)] (projectFirstRaw rtr)" 
    using Cons.IH tte wrtr 
    by linarith
  with wr rnonempty ftype show ?case 
    by (simp add: welltypedRawtable_def welltypedRow_def)
qed

lemma findColPreservesWelltypedRaw:
	assumes wrt: "welltypedRawtable tt rt"
  assumes mattrl: "matchingAttrL tt tal"
	assumes fct: "findColType a tt = Some ct" 
	assumes "findCol a tal rt = Some rt1"
  shows "welltypedRawtable [(a, ct)] rt1"
  using assms
proof (induction tal arbitrary: tt rt)
  case Nil
  then show ?case 
    by (simp add: welltypedRawtable_def)
next
  case (Cons a1 talr)
  then show ?case 
  proof (cases "a = a1")
    case True
    have rt1e: "rt1 = projectFirstRaw rt" 
      using Cons.prems(4) True  
      by auto
    obtain ttr where ttre: "tt = (a, ct)#ttr" 
      by (smt Cons.prems(2) Cons.prems(3) True TypedTables.findColType.elims TypedTables.matchingAttrL_def list.inject list.map(2) option.distinct(1) option.inject prod.sel(1))
    show ?thesis 
      using Cons.prems(1) projectFirstRawPreservesWelltypedRaw rt1e ttre 
      by fastforce
  next
    case False
    obtain tth ttr where ttre: "tt = tth # ttr" and mar: "matchingAttrL ttr talr" 
      by (smt Cons.prems(2) TypedTables.matchingAttrL_def length_Suc_conv list.inject list.map(2)) 
    obtain rtr where drtr: "rtr = dropFirstColRaw rt" 
      by simp
    have wtrr: "welltypedRawtable ttr rtr" 
      using Cons.prems(1) dropFirstColRawPreservesWelltypedRaw drtr ttre 
      by blast
    have fctr: "findColType a ttr = Some ct" 
      by (smt Cons.prems(2) Cons.prems(3) False TypedTables.findColType.elims TypedTables.matchingAttrL_def list.inject list.map(2) option.distinct(1) prod.sel(1) ttre)
    have fc: "findCol a talr rtr = Some rt1"
      using Cons.prems(4) False drtr 
      by auto
    show ?thesis 
      using Cons.IH fc fctr mar wtrr 
      by blast
      
qed
qed


lemma attachColToFrontRawPreservesWelltypedRaw:
  assumes tt1eq: "tt1 = ct#Nil"
  assumes "length rt1 = length rt2"
  assumes "welltypedRawtable tt1 rt1"
  assumes "welltypedRawtable tt2 rt2"
  shows "welltypedRawtable (ct#tt2) (attachColToFrontRaw rt1 rt2)"
  using assms
proof (induction rt1 arbitrary: rt2)
  case Nil
  then show ?case 
    by (simp add: welltypedRawtable_def)
next
  case (Cons r rt1r)
  obtain r' rt2r where 
      lrt1rrt2r: "length rt1r = length rt2r" and rtrr: "rt2 = r'#rt2r" 
    by (metis Cons.prems(2) length_Suc_conv)
  from tt1eq Cons.prems(3) have wr: "welltypedRow [ct] r" 
    by (meson TypedTables.welltypedRawtable_def filter_id_conv list.set_intros(1))
  obtain f where fnil: "r = f#Nil" 
    by (metis TypedTables.welltypedRow_def length_0_conv length_Suc_conv wr)
  have wrt1r: "welltypedRawtable [ct] rt1r" 
    by (metis Cons.prems(3) TypedTables.welltypedRawtable_def filter.simps(2) list.inject tt1eq wr)
  have wrt2r: "welltypedRawtable tt2 rt2r"
    by (smt Cons.prems(4) TypedTables.welltypedRawtable_def filter.simps(2) filter_eq_ConsD list.inject rtrr)
  from Cons.prems(4) rtrr have wrp: "welltypedRow tt2 r'" 
    by (simp add: welltypedRawtable_def, meson filter_eq_ConsD)
  have "welltypedRawtable (ct # tt2) (attachColToFrontRaw rt1r rt2r)" 
    by (simp add: Cons.IH lrt1rrt2r tt1eq wrt1r wrt2r)
  with wrp fnil rtrr wr show ?case 
    by (simp add: welltypedRawtable_def welltypedRow_def)
qed

lemma attachColToFrontRawPreservesRowCount:
  assumes "length rt1 = length rt2"
  assumes "welltypedRawtable [ct] rt1"
  shows "length rt1 = length (attachColToFrontRaw rt1 rt2)"
  using assms
proof (induction rt1 arbitrary: rt2)
  case Nil
  then show ?case 
    by simp
next
  case (Cons r rt1r)
  obtain r' rt2r where 
      lrt1rrt2r: "length rt1r = length rt2r" and rtrr: "rt2 = r'#rt2r" 
    by (metis Cons.prems(1) length_Suc_conv)
  have wr: "welltypedRow [ct] r" 
    by (meson Cons.prems(2) TypedTables.welltypedRawtable_def filter_eq_ConsD)
  obtain f where fnil: "r = f#Nil" 
    by (metis TypedTables.welltypedRow_def length_0_conv length_Suc_conv wr)
  have wrt1r: "welltypedRawtable [ct] rt1r" 
    by (metis Cons.prems(2) TypedTables.welltypedRawtable_def filter.simps(2) list.inject wr)
  have ll: "length rt1r = length (attachColToFrontRaw rt1r rt2r)"
    using Cons.IH lrt1rrt2r wrt1r 
    by blast
  show ?case 
    by (simp add: fnil ll rtrr)
qed

lemma projectFirstRawPreservesRowCount:
  shows "length rt = length (projectFirstRaw rt)"
proof (induction rt)
  case Nil
  then show ?case 
    by simp
next
  case (Cons r rtr)
  then show ?case 
    by (smt list.inject list.size(4) projectFirstRaw.elims)
qed

lemma dropFirstColRawPreservesRowCount:
  shows "length rt = length (dropFirstColRaw rt)"
proof (induction rt)
  case Nil
  then show ?case 
    by simp
next
  case (Cons r rtr)
  then show ?case
    by (smt dropFirstColRaw.elims length_Cons list.sel(3))
qed


lemma findColPreservesRowCount:
  assumes "findCol a al rt1 = Some rt2"
  shows "length rt1 = length rt2"
  using assms
proof (induction al arbitrary: rt1)
  case Nil
  then show ?case 
    by simp
next
  case (Cons a1 alr)
  then show ?case 
  proof (cases "a = a1")
    case True
    show ?thesis 
      using Cons.prems True projectFirstRawPreservesRowCount 
      by auto
  next
    case False
    obtain drt where drteq: "drt = dropFirstColRaw rt1" 
      by auto
    have fc: "findCol a alr drt = Some rt2" 
      using Cons.prems False drteq 
      by auto
    have ldrt: "length drt = length rt2" 
      using Cons.IH fc 
      by blast
    have ll: "length rt1 = length drt" 
      using dropFirstColRawPreservesRowCount drteq 
      by blast
    show ?thesis 
      by (simp add: ldrt ll)
  qed
qed

lemma projectEmptyColPreservesRowCount:
  shows "length rt = length (projectEmptyCol rt)"
proof (induction rt)
case Nil
  then show ?case 
    by simp
next
  case (Cons r rtr)
  then show ?case 
    by simp
qed



lemma projectColsPreservesRowCount:
  assumes ptattrl: "projectTypeAttrL al1 tt = Some tt'"
  assumes pcrt1: "projectCols al1 al2 rt1 = Some rt2"
  assumes wrt: "welltypedRawtable tt rt1"
  assumes mattrl: "matchingAttrL tt al2"
  shows "length rt1 = length rt2"
  using assms
proof (induction al1 arbitrary: rt2 tt')
  case Nil
  show ?case 
    using Nil.prems(2) projectEmptyColPreservesRowCount 
    by auto
next
  case (Cons a alr)
  then show ?case 
  proof -
    obtain tt'' where ptattrlttpp: "projectTypeAttrL alr tt = Some tt''" 
      by (metis (no_types, lifting) Cons.prems(1) option.case_eq_if 
            option.collapse projectTypeAttrL.simps(2))
    obtain ct where fct: "findColType a tt = Some ct"
      using Cons.prems(1) 
      by fastforce
    obtain rt1' where rt1p: "findCol a al2 rt1 = Some rt1'" 
      using Cons.prems(2) 
      by fastforce
    obtain rt2' where rt2p: "projectCols alr al2 rt1 = Some rt2'" 
      by (metis (no_types, lifting) Cons.prems(2) option.case_eq_if option.collapse projectCols.simps(2))
    have res: "rt2 = attachColToFrontRaw rt1' rt2'" 
      using Cons.prems(2) rt1p rt2p 
      by auto
    have lrt2: "length rt1 = length rt2'" 
      using Cons.IH Cons.prems(3) \<open>\<And>thesis. (\<And>tt''. projectTypeAttrL alr tt = Some tt'' \<Longrightarrow> thesis) \<Longrightarrow> thesis\<close> mattrl rt2p 
      by blast
    have lrt1: "length rt1 = length rt1'" 
      by (meson TypedTables.findColPreservesRowCount rt1p)
    have wrt1: "welltypedRawtable [(a, ct)] rt1'" 
      by (meson TypedTables.findColPreservesWelltypedRaw fct mattrl rt1p wrt)
    show ?case
      by (metis attachColToFrontRawPreservesRowCount lrt1 lrt2 res wrt1)
  qed
qed

lemma projectColsWelltypedWithSelectType:
  assumes wrt: "welltypedRawtable tt rt"
  assumes mattrl: "matchingAttrL tt tal"
  assumes pta: "projectTypeAttrL al tt = Some tt'"
  assumes pc: "projectCols al tal rt = Some rt'"
  shows "welltypedRawtable tt' rt'"
  using assms
proof (induction al arbitrary: tt' rt')
  case Nil
  with welltypedEmptyProjection show ?case 
     by auto
next
  case (Cons a alr)
  then show ?case
  proof - 
    obtain tt'' where  ptattrlttpp: "projectTypeAttrL alr tt = Some tt''" 
      by (metis (no_types, lifting) Cons.prems(3) 
           option.case_eq_if option.collapse projectTypeAttrL.simps(2))
    obtain ct where fct: "findColType a tt = Some ct" 
      using Cons.prems(3) 
      by fastforce
    have ttpp: "tt' = (a, ct) # tt''" 
      using Cons.prems(3) fct ptattrlttpp 
      by auto
    obtain tta where ttaeq: "tta = (a, ct)#Nil" 
      by simp
    obtain rt1 where fcrt1: "findCol a tal rt = Some rt1" 
      using Cons.prems(4) 
      by fastforce
    obtain rt2 where pcrt2: "projectCols alr tal rt = Some rt2" 
      by (metis (no_types, lifting) Cons.prems(4) option.case_eq_if option.collapse projectCols.simps(2))
    have wrt1: "welltypedRawtable tta rt1" 
      using TypedTables.findColPreservesWelltypedRaw fcrt1 fct mattrl ttaeq wrt 
      by fastforce
    have wrt2: "welltypedRawtable tt'' rt2" 
      by (simp add: Cons.IH mattrl pcrt2 ptattrlttpp wrt)
    have lrt1: "length rt1 = length rt" 
      using TypedTables.findColPreservesRowCount fcrt1 
      by force
    have lrt2: "length rt2 = length rt" 
      using mattrl pcrt2 projectColsPreservesRowCount ptattrlttpp wrt 
      by fastforce
    have attch: "rt' = attachColToFrontRaw rt1 rt2" 
      using Cons.prems(4) fcrt1 pcrt2 
      by auto
    show ?thesis 
      by (metis TypedTables.attachColToFrontRawPreservesWelltypedRaw attch lrt1 lrt2 ttaeq ttpp wrt1 wrt2)
   qed
qed

lemma projectTableWelltypedWithSelectType: 
  assumes wrt: "welltypedtable tt t"
  assumes projType: "projectType sel tt = Some tt'"
  assumes projTable: "projectTable sel t = Some t'"
  shows "welltypedtable tt' t'"
  using assms
proof (cases sel)
  case all
  then show ?thesis
    using projTable projType wrt 
    by auto
next
  case (list al)
  then show ?thesis 
  proof -
    have pt: "projectTypeAttrL al tt = Some tt'" 
      using list projType 
      by auto
    from list projTable obtain rt2 where pcrt2:  "projectCols al (getAttrL t) (getRaw t) = Some rt2"
      by fastforce
    have  tp: "t' = table al rt2" 
      using list pcrt2 projTable 
      by auto 
    have wrt2: "welltypedRawtable tt' rt2" 
      using TypedTables.projectColsWelltypedWithSelectType pcrt2 pt welltypedtable_def wrt 
      by blast 
    have ma: "matchingAttrL tt' al" 
      using list projTable projectTypeAttrLMatchesProjectTableAttrL pt tp 
        welltypedtable_def wrt 
      by fastforce
    show ?thesis 
      by (simp add: ma tp welltypedtable_def wrt2)
  qed 
qed

text_raw {*\DefineSnippet{PreservationTheorem}{*}
theorem SQLPreservation:
assumes typable: "TTC \<turnstile> q : TT"
assumes step: "reduce q TS = Some q'"
assumes consistency: "StoreContextConsistent TS TTC"
shows "TTC \<turnstile> q' : TT"
  using assms
proof (induction q arbitrary: q')
text_raw {*}%EndSnippet*}
case (tvalue t)
  then show ?case
    by simp
next
text_raw {*\DefineSnippet{PreservationSelectFromWhereCase}{*}
  case (selectFromWhere s tnl p)
  then show ?case
    using assms
  proof (cases "length tnl = 1")
    case True
    then show ?thesis
    proof -
      obtain n where ai: "tnl = n # Nil" 
        using selectFromWhere(1) selectFromWhere_INV 
        by blast
      obtain tt' where lnttc: "lookupEnv n TTC = Some tt'" 
        using ai selectFromWhere.prems(1) selectFromWhere_INV 
        by fastforce
      obtain t where lnts: "lookupEnv n TS = Some t" 
        by (meson consistency lnttc successfulLookup) 
      obtain tsel where ts: "q' = tvalue tsel" 
        using reduce_INV_selectFromWhere selectFromWhere.prems(2) 
        by blast
      obtain ft where ftp: "filterTable t p = ft" 
        by simp
      have pt: "projectTable s ft = Some tsel" 
        using ai ftp lnts reduce_INV_selectFromWhere selectFromWhere.prems(2) ts 
        by fastforce
      have ptt: "projectType s tt' = Some TT" 
        using ai lnttc selectFromWhere.prems(1) selectFromWhere_INV 
        by fastforce
      have wtt: "welltypedtable tt' t" 
        by (meson consistency lnts lnttc welltypedLookup)
      have wtft: "welltypedtable tt' ft" 
        using wtt filterPreservesType ftp 
        by blast
      have wtsel: "welltypedtable TT tsel" 
        using projectTableWelltypedWithSelectType pt ptt wtft 
        by blast
      show ?thesis
        by (metis Table.exhaust Ttvalue ts wtsel)
      qed
    next
    case False
    show ?thesis 
      using False selectFromWhere.prems(2) 
      by auto
  qed
text_raw {*}%EndSnippet*}
next
text_raw {*\DefineSnippet{PreservationUnionCase}{*}
  case (union q1 q2)
  have wtq1: "TTC \<turnstile> q1 : TT" 
    using union.prems(1) union_INV 
    by blast
  have wtq2: "TTC \<turnstile> q2 : TT" 
    using union.prems(1) union_INV 
    by blast
  then show ?case 
  proof (cases "isValue q1")
      case valueq1t: True 
        then show ?thesis 
        proof (cases "isValue q2")
          case valueq2t: True
           text {*...*}
          (*<*)
          obtain al1 rt1 where tab1: "q1 = tvalue (table al1 rt1)" 
            using isValue_true_INV valueq1t 
            by blast
          obtain al2 rt2 where tab2: "q2 = tvalue (table al2 rt2)" 
            using isValue_true_INV valueq2t 
            by blast
          have wt1: "welltypedtable TT (table al1 rt1)" 
            using tab1 tvalue_INV wtq1 
            by blast
          have wt2: "welltypedtable TT (table al2 rt2)"
            using tab2 tvalue_INV wtq2 
            by blast
          have wrt1: "welltypedRawtable TT rt1" 
            using welltypedtable_def wt1 
            by force
          have wrt2: "welltypedRawtable TT rt2" 
            using welltypedtable_def wt2 
            by force
          have wtrt: "welltypedRawtable TT (rawUnion rt1 rt2)" 
            using rawUnionPreservesWelltypedRaw wrt1 wrt2 
            by blast
          have mattrl: "matchingAttrL TT al1" 
            using welltypedtable_def wt1 
            by force
          have wtr: "welltypedtable TT (table al1 (rawUnion rt1 rt2))" 
            by (simp add: mattrl welltypedtable_def wtrt)
          have qp: "q' = tvalue (table al1 (rawUnion rt1 rt2))"
            by (metis (no_types, lifting) Query.case(1) Table.reduce.simps(3) 
                getAttrL.simps getRaw.simps getTable_def option.inject tab1 tab2 
                union.prems(2) valueq1t valueq2t)
          show ?thesis 
            by (simp add: Ttvalue qp wtr)
        (*>*)
        next
          case valueq2f: False
          then show ?thesis 
            proof - 
               obtain q where redq2: "reduce q2 TS = Some q" and red: "q' = union q1 q"
                 using reduce_INV_union_valueq1t_valueq2f union.prems(2) valueq1t valueq2f 
                 by blast
               have wt: "TTC \<turnstile> q : TT" 
                 by (simp add: consistency redq2 union.IH(2) wtq2)
               show ?thesis 
                 by (simp add: TUnion red wt wtq1)
           qed
         qed
       next
         case valueq1f: False
          text {* ... *}
          (*<*)
         then show ?thesis 
          proof - 
            obtain q where redq1: "reduce q1 TS = Some q" and red: "q' = union q q2"
              using reduce_INV_union_valueq1f union.prems(2) valueq1f 
              by blast
            have wt: "TTC \<turnstile> q : TT" 
              by (simp add: consistency redq1 union.IH(1) wtq1)
            show ?thesis 
              by (simp add: TUnion red wt wtq2)
          qed
          (*>*)
        qed
text_raw {*}%EndSnippet*}
next
  case (intersection q1 q2)
  have wtq1: "TTC \<turnstile> q1 : TT" 
    using intersection.prems(1) intersection_INV 
    by blast
  have wtq2: "TTC \<turnstile> q2 : TT" 
    using intersection.prems(1) intersection_INV 
    by blast
  then show ?case 
  proof (cases "isValue q1")
      case valueq1t: True 
        then show ?thesis 
        proof (cases "isValue q2")
          case valueq2t: True
          obtain al1 rt1 where tab1: "q1 = tvalue (table al1 rt1)" 
            using isValue_true_INV valueq1t 
            by blast
          obtain al2 rt2 where tab2: "q2 = tvalue (table al2 rt2)" 
            using isValue_true_INV valueq2t 
            by blast
          have wt1: "welltypedtable TT (table al1 rt1)"
            using tab1 tvalue_INV wtq1 
            by blast 
          have wt2: "welltypedtable TT (table al2 rt2)"
            using tab2 tvalue_INV wtq2 
            by blast
          have wrt1: "welltypedRawtable TT rt1"
            using welltypedtable_def wt1 
            by force
          have wrt2: "welltypedRawtable TT rt2" 
            using welltypedtable_def wt2 by force
          have wtrt: "welltypedRawtable TT (rawIntersection rt1 rt2)" 
            by (simp add: rawIntersectionPreservesWelltypedRaw wrt1 wrt2)
          have ma: "matchingAttrL TT al1"
            using welltypedtable_def wt1 
            by force
          have wtr: "welltypedtable TT (table al1 (rawIntersection rt1 rt2))" 
            by (simp add: ma welltypedtable_def wtrt)
          have qp: "q' = tvalue (table al1 (rawIntersection rt1 rt2))"
            by (metis (no_types, lifting) Query.case(1) getAttrL.simps getRaw.simps getTable_def intersection.prems(2) option.inject reduce.simps(4) tab1 tab2 valueq1t valueq2t)
          show ?thesis 
            by (simp add: Ttvalue qp wtr)
        next
          case valueq2f: False
          then show ?thesis 
            proof -
               obtain q where redq2: "reduce q2 TS = Some q" and red: "q' = intersection q1 q"
                 using intersection.prems(2) reduce_INV_intersection_valueq1t_valueq2f valueq1t valueq2f 
                 by blast
               have wt: "TTC \<turnstile> q : TT" 
                 by (simp add: consistency intersection.IH(2) redq2 wtq2)
               show ?thesis
                 by (simp add: TIntersection red wt wtq1)
           qed
         qed
       next
         case valueq1f: False
         then show ?thesis 
          proof - 
            obtain q where redq1: "reduce q1 TS = Some q" and red: "q' = intersection q q2"
              using intersection.prems(2) reduce_INV_intersection_valueq1f valueq1f 
              by blast
            have tt: "TTC \<turnstile> q : TT" 
              using consistency intersection.IH(1) redq1 wtq1 
              by blast
            show ?thesis 
              by (simp add: TIntersection tt red wtq2)
          qed
       qed
next
  case (difference q1 q2)
  have wtq1: "TTC \<turnstile> q1 : TT" 
    using difference.prems(1) difference_INV 
    by blast
  have wtq2: "TTC \<turnstile> q2 : TT" 
    using difference.prems(1) difference_INV 
    by blast
  then show ?case 
  proof (cases "isValue q1")
      case valueq1t: True 
        then show ?thesis 
        proof (cases "isValue q2")
          case valueq2t: True
          obtain al1 rt1 where tab1: "q1 = tvalue (table al1 rt1)" 
            using isValue_true_INV valueq1t 
            by blast
          obtain al2 rt2 where tab2: "q2 = tvalue (table al2 rt2)" 
            using isValue_true_INV valueq2t 
            by blast
          have wt1: "welltypedtable TT (table al1 rt1)" 
            using tab1 tvalue_INV wtq1 
            by blast
          have wt2: "welltypedtable TT (table al2 rt2)"
            using tab2 tvalue_INV wtq2 
            by auto
          have wrt1: "welltypedRawtable TT rt1" 
            using welltypedtable_def wt1 
            by force
          have wrt2: "welltypedRawtable TT rt2" 
            using welltypedtable_def wt2 
            by force
          have wtrt: "welltypedRawtable TT (rawDifference rt1 rt2)" 
            by (simp add: rawDifferencePreservesWelltypedRaw wrt1 wrt2)
          have ma: "matchingAttrL TT al1" 
            using welltypedtable_def wt1 
            by force
          have wtr: "welltypedtable TT (table al1 (rawDifference rt1 rt2))"
            by (simp add: ma welltypedtable_def wtrt)
          have qp: "q' = tvalue (table al1 (rawDifference rt1 rt2))"
            by (metis (no_types, lifting) Query.case(1) difference.prems(2) getAttrL.simps getRaw.simps getTable_def option.inject reduce.simps(5) tab1 tab2 valueq1t valueq2t)
          show ?thesis 
            using Ttvalue qp wtr 
            by blast
        next
          case valueq2f: False
          then show ?thesis 
            proof -
              obtain q where redq2: "reduce q2 TS = Some q" and red: "q' = difference q1 q"
                using difference.prems(2) reduce_INV_difference_valueq1t_valueq2f valueq1t valueq2f 
                by blast
              have tt: "TTC \<turnstile> q : TT" 
                by (simp add: consistency difference.IH(2) redq2 wtq2)
              show ?thesis
                using TDifference red tt wtq1 
                by blast
           qed
         qed
       next
         case valueq1f: False
         then show ?thesis 
          proof - 
            obtain q where redq1: "reduce q1 TS = Some q" and red: "q' = difference q q2"
              using difference.prems(2) reduce_INV_difference_valueq1f valueq1f 
              by blast
            have tt: "TTC \<turnstile> q : TT" 
              using consistency difference.IH(1) redq1 wtq1 
              by blast
            show ?thesis 
              by (simp add: TDifference red tt wtq2)
          qed
       qed
qed

end

end