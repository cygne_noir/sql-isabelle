theory ProgressProof
imports SQLSemantics SQLTypeSystem SoundnessAuxDefs
begin

context TypedTables
begin

lemma successfulLookup: 
  assumes consistency: "StoreContextConsistent ts ttc"
  assumes lookup: "\<exists> tt. lookupEnv ref ttc = Some tt"
  shows "\<exists> t. lookupEnv ref ts = Some t"
  using assms   
proof (induct arbitrary: ttc)
  case (empty)
  then show ?case 
    using StoreContextConsistent.elims(2) 
    by force 
next
  case (bind tn tab tsr)
  then show ?case 
    by (smt Env.distinct(1) Env.inject StoreContextConsistent.elims(2) 
        lookupEnv.simps(2))
qed

lemma welltypedLookup:
  assumes consistency: "StoreContextConsistent ts ttc"
  assumes type_lookup: "lookupEnv ref ttc = Some tt"
  assumes store_lookup: "lookupEnv ref ts = Some t"
  shows "(welltypedtable tt t)"
  using assms
proof (induct arbitrary: ttc)
  case empty
  then show ?case 
    by simp
next
  case (bind tn tab tsr)
  then show ?case
    by (smt Env.distinct(1) Env.inject StoreContextConsistent.elims(2) 
        lookupEnv.simps(2) option.inject)
qed


lemma filterDoesNotInventRows:
  assumes infilter: "r \<in> set (filterRows rt al p)"
  shows "r \<in> set rt"
  using assms
proof (induct rt)
  case Nil
  thus ?case 
    by auto
next
  case (Cons r rt)
  thus ?case
    by (smt filterRows.simps(2) list.set_intros(1) list.set_intros(2) set_ConsD)
qed

lemma filterRowsPreservesTable:
  assumes welltyped: "welltypedRawtable tt rt"
  shows "welltypedRawtable tt (filterRows rt al p)"
proof -
  have rtr: "\<forall> r \<in> (set rt). welltypedRow tt r" 
    by (meson TypedTables.welltypedRawtable_def filter_id_conv welltyped)
  have "\<forall> r \<in> (set (filterRows rt al p)). welltypedRow tt r" 
    using filterDoesNotInventRows rtr 
    by blast
  then show ?thesis
    using TypedTables.welltypedRawtable_def filter_True 
    by blast
qed

lemma filterPreservesType:
  assumes welltyped: "welltypedtable tt t"
  shows "welltypedtable tt (filterTable t p)"
  by (metis Table.exhaust Table.getAttrL.simps Table.getRaw.simps 
      filterRowsPreservesTable filterTable.simps welltyped welltypedtable_def)

lemma dropFirstColRawPreservesWelltypedRaw:
  assumes noemptytt: "tt = tth#ttl "
  assumes welltypedtt: "welltypedRawtable tt rt"
  shows "welltypedRawtable ttl (dropFirstColRaw rt)"
  using assms
proof (induction rt)
  case Nil
  then show ?case
    by (simp add: welltypedRawtable_def)
next
  case (Cons a rtr)
  then show ?case
  proof- 
    have a1: "welltypedRow tt a"
      by (meson Cons.prems(2) filter_eq_ConsD welltypedRawtable_def)
    obtain a1 ar where vars: "a = a1 # ar"
      by (metis a1 length_Suc_conv noemptytt welltypedRow_def)
    with Cons.prems(1) a1 have a2: "welltypedRow ttl ar" 
      by (simp add: welltypedRawtable_def welltypedRow_def)
    have a3: "welltypedRawtable tt rtr"
      by (metis Cons.prems(2) a1 filter.simps(2) list.inject welltypedRawtable_def)
    have a4: "welltypedRawtable ttl (dropFirstColRaw rtr)" 
      using Cons.IH a3 noemptytt 
      by auto
    have a5: "dropFirstColRaw (a # rtr) = ar # (dropFirstColRaw rtr)" 
      by (simp add: vars)
    show ?case
      by (metis (full_types) a2 a4 a5 filter.simps(2) welltypedRawtable_def)
  qed
qed
 
lemma findColTypeImpliesfindCol:
  assumes welltyped: "welltypedtable tt (table al rt)"
  assumes findCT: "findColType a tt = Some ftype"
  shows "\<exists> t'. findCol a al rt = Some t'"
using assms
proof (induction a al rt arbitrary: tt rule: findCol.induct)
  case (1 n rt)
  hence "tt == []" by (simp add: welltypedtable_def matchingAttrL_def)
  show ?case 
    using "1.prems"(2) \<open>tt \<equiv> []\<close> 
    by auto
next
  case (2 n a al rt)
  then show ?case 
  proof (cases "n \<noteq> a")
    case False
    then show ?thesis by auto
  next
    case True
    show ?thesis
    proof -
      obtain ta ttr where a0: "tt = ta#ttr"
        by (metis "2.prems"(2) TypedTables.findColType.elims option.distinct(1))
      have wrt: "welltypedRawtable tt rt" 
        using "2.prems"(1) welltypedtable_def by force
      have a1p: "welltypedRawtable ttr (dropFirstColRaw rt)" 
        using TypedTables.dropFirstColRawPreservesWelltypedRaw a0 wrt 
        by blast
      have mattr: "matchingAttrL ttr al" 
        by (metis (no_types, lifting) "2.prems"(1) a0 getAttrL.simps length_Suc_conv list.inject list.map(2) matchingAttrL_def welltypedtable_def)
      have a1: "welltypedtable ttr (table al (dropFirstColRaw rt))"
        by (simp add: a1p mattr welltypedtable_def)
      have tan: "(fst ta) \<noteq> n" 
        by (metis "2.prems"(1) True a0 getAttrL.simps list.inject list.map(2) matchingAttrL_def welltypedtable_def)
      have a3: "findColType n ttr = Some ftype" 
        by (smt "2.prems"(2) TypedTables.findColType.elims a0 fst_conv list.inject option.distinct(1) tan)
      obtain t' where IHconc: "findCol n al (dropFirstColRaw rt) = Some t'"
        using "2.IH" True a1 a3 
        by blast
      with True show ?thesis 
        by auto
    qed
  qed
qed
    
lemma projectTypeImpliesfindCol:
  assumes welltyped: "welltypedtable tt (table al rt)"
  assumes projTypeAttrL: "projectTypeAttrL (a#alr) tt = Some tt'"
  shows "\<exists> t'. findCol a al rt = Some t'"
proof - 
  obtain fct where fcte: "findColType a tt = Some fct" 
    using assms(2) 
    by fastforce
  show ?thesis 
    by (meson TypedTables.findColTypeImpliesfindCol fcte welltyped)
qed

lemma projectColsProgress:
  assumes welltyped: "welltypedtable tt (table alt rt)"
  assumes projectType: "projectType (list al) tt = Some tt'"
  shows "\<exists> t'. (projectCols al alt rt) = Some t'"
  using assms
proof (induct al arbitrary: tt')
  case Nil
  then show ?case 
    by auto
next
  case (Cons n nal)
  obtain tt' where projtyperest: "projectType (list nal) tt = Some tt'" 
    by (metis (no_types, lifting) Cons.prems(2) option.case_eq_if 
         option.collapse projectType.simps(2) projectTypeAttrL.simps(2))
  obtain t' where tp: "findCol n alt rt = Some t'" 
    using Cons.prems(2) TypedTables.projectTypeImpliesfindCol welltyped 
    by fastforce 
  obtain t'' where tpp: "projectCols nal alt rt = Some t''" 
    using Cons.hyps \<open>\<And>thesis. (\<And>tt'. projectType (list nal) tt = Some tt' \<Longrightarrow> thesis) \<Longrightarrow> thesis\<close> welltyped 
    by auto 
  show ?case 
    by (simp add: tp tpp)
qed

lemma projectTableProgress:
  assumes welltyped: "welltypedtable tt t"
  assumes projectType: "projectType s tt = Some tt'"
  shows "\<exists> t'. projectTable s t = Some t'"
  using assms
proof (cases s)
  case all
  then show ?thesis 
    by auto
next
  case (list x)
  obtain al rt where tab: "t = (table al rt)" 
    by (rule Tables.Table.exhaust, auto)
  show ?thesis 
    using TypedTables.projectColsProgress list projectType tab welltyped 
    by fastforce
qed

text_raw {*\DefineSnippet{ProgressTheorem}{*}
theorem SQLProgress:
  assumes noValue: "\<not> (isValue  q)"
  assumes typable: "TTC \<turnstile> q : TT"
  assumes consistency: "StoreContextConsistent TS TTC"
  shows "\<exists> q'. reduce q TS = Some q'"
  using assms
proof (induction q) 
text_raw {*}%EndSnippet*}
  case (tvalue t)
  then show ?case 
    by (simp add: isValue_def)
next
text_raw {*\DefineSnippet{ProgressSelectFromWhereCase}{*}
  case (selectFromWhere s tnl p)
  then show ?case
  proof (cases "length tnl = 1")
    case True
    obtain tt where lTT: "lookupEnv (hd tnl) TTC = Some tt" 
      by (metis list.sel(1) selectFromWhere.prems(2) selectFromWhere_INV)
    obtain t where ltable: "lookupEnv (hd tnl) TS = Some t"
      by (meson TypedTables.successfulLookup consistency lTT)
    have wtt: "welltypedtable tt t" 
      by (meson TypedTables.welltypedLookup consistency lTT ltable)
    have wft: "welltypedtable tt (filterTable t p)" 
      by (simp add: filterPreservesType wtt)
    obtain tsel where pft: "projectTable s (filterTable t p) = Some tsel"
      by (metis TypedTables.selectFromWhere_INV lTT list.sel(1) 
            option.inject projectTableProgress selectFromWhere.prems(2) wft)
    show ?thesis 
      by (simp add: True ltable pft)
  next
    case False
    thus ?thesis 
      by (metis One_nat_def length_Cons list.size(3) selectFromWhere.prems(2) 
          selectFromWhere_INV)
  qed
text_raw {*}%EndSnippet*}
next
text_raw {*\DefineSnippet{ProgressUnionCase}{*}
  case (union q1 q2)
  then show ?case
    proof (cases "isValue q1")
      case q1t: True
      then show ?thesis
        proof (cases "isValue q2")
          case q2t: True
          with q1t show ?thesis 
            by simp 
        next
          case q2f: False
          have tq2: "TTC \<turnstile> q2 : TT" 
            using TypedTables.union_INV union.prems(2) 
            by blast
          obtain q2' where rq2: "reduce q2 TS = Some q2'" 
            using consistency q2f tq2 union.IH(2) 
            by blast
          obtain t where tv: "q1 = tvalue t"   
            using isValue_true_INV q1t 
            by blast
          with rq2 q2f show ?thesis 
            using q1t 
            by auto
        qed
      next
        case False  
        text {*...*}
      (*<*)
      have tt: "TTC \<turnstile> q1 : TT" 
        using TypedTables.union_INV union.prems(2) 
        by blast
      obtain q1' where "reduce q1 TS = Some q1'" 
        using False consistency tt union.IH(1) 
        by blast
      with False show ?thesis 
        by (auto simp add: isValue_def)
      (*>*)
    qed
text_raw {*}%EndSnippet*}
next
  case (intersection q1 q2)
  then show ?case
    proof (cases "isValue q1")
      case q1t: True
      then show ?thesis
        proof (cases "isValue q2")
          case q2t: True
          with q1t show ?thesis 
            by simp 
        next
          case q2f: False
          have tq2: "TTC \<turnstile> q2 : TT" 
            using TypedTables.intersection_INV intersection.prems(2) 
            by blast
          obtain q2' where rq2: "reduce q2 TS = Some q2'" 
            using consistency q2f tq2 intersection.IH(2) 
            by blast
          obtain t where tv: "q1 = tvalue t"   
            using isValue_true_INV q1t 
            by blast
          with rq2 q2f show ?thesis 
            using q1t 
            by auto
        qed
    next
      case False
      have tt: "TTC \<turnstile> q1 : TT" 
        using TypedTables.intersection_INV intersection.prems(2) 
        by blast
      obtain q1' where "reduce q1 TS = Some q1'" 
        using False consistency tt intersection.IH(1) 
        by blast
      with False show ?thesis 
        by (auto simp add: isValue_def)
    qed
next
  case (difference q1 q2)
  then show ?case
    proof (cases "isValue q1")
      case q1t: True
      then show ?thesis
        proof (cases "isValue q2")
          case q2t: True
          with q1t show ?thesis 
            by simp 
        next
          case q2f: False
          have tq2: "TTC \<turnstile> q2 : TT" 
            using TypedTables.difference_INV difference.prems(2) 
            by blast
          obtain q2' where rq2: "reduce q2 TS = Some q2'" 
            using consistency q2f tq2 difference.IH(2) 
            by blast
          obtain t where tv: "q1 = tvalue t"   
            using isValue_true_INV q1t 
            by blast
          with rq2 q2f show ?thesis 
            using q1t 
            by auto
        qed
    next
      case False
      have tt: "TTC \<turnstile> q1 : TT" 
        using TypedTables.difference_INV difference.prems(2) 
        by blast
      obtain q1' where "reduce q1 TS = Some q1'" 
        using False consistency tt difference.IH(1) 
        by blast
      with False show ?thesis 
        by (auto simp add: isValue_def)
    qed
qed

end

end