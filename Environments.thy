theory Environments
  imports Main
begin

text_raw {*\DefineSnippet{Environments}{*}
datatype ('id, 'a) Env = empty | bind 'id 'a "('id, 'a) Env"

primrec lookupEnv :: "'id \<Rightarrow> ('id, 'a) Env \<Rightarrow> 'a option"
where
"lookupEnv n empty = None" |
"lookupEnv n (bind m a e) = (if (n = m)
  then (Some a)
  else (lookupEnv n e))"
text_raw {*}%EndSnippet*}

end