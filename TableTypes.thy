theory TableTypes
imports Tables
begin

text_raw {*\DefineSnippet{TableTypesBasicStructures}{*}
type_synonym ('id, 'ftype) TType = "('id \<times> 'ftype) list"

locale FieldTypes = 
fixes fieldType :: "'val \<Rightarrow> 'ftype"
(* underspecified function for assigning field types to table cell values*)

locale TypedTables = Table lt gt + FieldTypes ft
for lt :: "'val \<Rightarrow> 'val \<Rightarrow> bool"
and gt :: "'val \<Rightarrow> 'val \<Rightarrow> bool"
and ft :: "'val \<Rightarrow> 'ftype"
begin
text_raw {*}%EndSnippet*}

text_raw {*\DefineSnippet{welltypedtable}{*}
definition matchingAttrL :: "('id, 'ftype) TType \<Rightarrow> 'id AttrL \<Rightarrow> bool"
where
"matchingAttrL tt al \<equiv> ((length tt) = (length al)) \<and>
  (map (\<lambda>tt. fst tt) tt) = al"

definition welltypedRow :: "('id, 'ftype) TType \<Rightarrow> 'val Row \<Rightarrow> bool"
where
"welltypedRow tt r \<equiv> ((length tt = (length r)) \<and>
  (map (\<lambda>v. ft v) r) = (map (\<lambda>tt. snd tt) tt))"

definition welltypedRawtable :: "('id, 'ftype) TType \<Rightarrow> 'val RawTable \<Rightarrow> bool"
where
"welltypedRawtable tt rt \<equiv> filter (\<lambda>r. welltypedRow tt r) rt = rt"

definition welltypedtable :: "('id, 'ftype) TType \<Rightarrow> ('id, 'val) Table \<Rightarrow> bool"
where                      
"welltypedtable tt t \<equiv> matchingAttrL tt (getAttrL t) \<and>
  welltypedRawtable tt (getRaw t)"
text_raw {*}%EndSnippet*}

end
end