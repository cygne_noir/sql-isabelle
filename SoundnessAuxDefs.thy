theory SoundnessAuxDefs
imports TableTypes Environments
begin

context TypedTables
begin

text_raw {*\DefineSnippet{StoreContextConsistent}{*}
fun StoreContextConsistent :: "('id, ('id, 'val) Table) Env \<Rightarrow> 
  ('id, ('id, 'ftype) TType) Env \<Rightarrow> bool"
where
"StoreContextConsistent empty empty = True" |
"StoreContextConsistent (bind tn1 t tsr) (bind tn2 tt ttcr) = 
  ((tn1 = tn2) \<and> welltypedtable tt t \<and> StoreContextConsistent tsr ttcr)" |
"StoreContextConsistent ts ttc = False"
text_raw {*}%EndSnippet*}

lemma StoreContextConsistent_INV: 
assumes "StoreContextConsistent ts ttc"
shows "(ts = empty \<and> ttc = empty) \<or>
  (\<exists> tn1 tn2 t tt tsr ttcr. 
    ts = bind tn1 t tsr \<and> ttc = bind tn2 tt ttcr
    \<and> tn1 = tn2 \<and> welltypedtable tt t \<and> StoreContextConsistent tsr ttcr)"
by (smt TypedTables.StoreContextConsistent.elims(2) assms)

end
end
