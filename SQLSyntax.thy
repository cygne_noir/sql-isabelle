theory SQLSyntax
imports Tables
begin

text_raw {*\DefineSnippet{QueriesDatastructures}{*}
datatype 'id Select = all | list "'id AttrL"

type_synonym 'id TRef = "'id list"

datatype ('id, 'val) Exp = const 'val | lookup 'id

datatype ('id, 'val) Pred = ptrue | 
  And "('id, 'val) Pred" "('id, 'val) Pred" |
  Not "('id, 'val) Pred" |
  Eq "('id, 'val) Exp" "('id, 'val) Exp" |
  Gt "('id, 'val) Exp" "('id, 'val) Exp" |
  Lt "('id, 'val) Exp" "('id, 'val) Exp"

datatype ('id, 'val) Query = tvalue "('id, 'val) Table" |
  selectFromWhere "'id Select" "'id TRef" "('id, 'val) Pred" |
  union "('id, 'val) Query" "('id, 'val) Query" |
  intersection "('id, 'val) Query" "('id, 'val) Query" |
  difference "('id, 'val) Query" "('id, 'val) Query"
text_raw {*}%EndSnippet*}

definition getTable :: "('id, 'val) Query \<Rightarrow> ('id, 'val) Table"
where
"getTable q \<equiv> (case q of 
  tvalue t \<Rightarrow> t |
  _ \<Rightarrow> table Nil Nil)"

definition isValue :: "('id, 'val) Query \<Rightarrow> bool"
where
"isValue q \<equiv> (case q of
  tvalue t \<Rightarrow> True |
  _ \<Rightarrow> False)"

lemma isValue_true_INV [simp]: "isValue q1 \<Longrightarrow> \<exists> al rt. q1 = tvalue (table al rt)"
proof -
  assume "isValue q1"
  from this obtain t where "q1 = tvalue t" 
    using isValue_def
    by (metis Query.exhaust Query.simps(27) Query.simps(28) Query.simps(29) Query.simps(30))
  thus "\<exists> al rt. q1 = tvalue (table al rt)" 
    using Table.exhaust 
    by blast
qed

end