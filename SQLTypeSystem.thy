theory SQLTypeSystem
imports TableTypes Environments SQLSyntax
begin

context TypedTables
begin

fun findColType :: "'id \<Rightarrow> ('id, 'ftype) TType \<Rightarrow> 'ftype option"
where
"findColType n Nil = None" |
"findColType n ((a, t)#ttr) = 
  (if a = n
    then Some t
    else findColType n ttr)"

primrec projectTypeAttrL :: "'id AttrL \<Rightarrow> ('id, 'ftype) TType \<Rightarrow> ('id, 'ftype) TType option"
where
"projectTypeAttrL Nil tt = Some Nil" |
"projectTypeAttrL (a#al) tt = 
    (let fct = findColType a tt in
      (let tprest = projectTypeAttrL al tt in
        (case fct of
          None \<Rightarrow> None |
          Some t \<Rightarrow> (case tprest of
            None \<Rightarrow> None |
            Some tt \<Rightarrow> Some ((a, t)#tt)))))"

primrec projectType :: "'id Select \<Rightarrow> ('id, 'ftype) TType \<Rightarrow> ('id, 'ftype) TType option"
where
"projectType all tt = Some tt" |
"projectType (list al) tt = projectTypeAttrL al tt"

fun typeOfExp :: "('id, 'val) Exp \<Rightarrow> ('id, 'ftype) TType \<Rightarrow> 'ftype option"
where
"typeOfExp (const fv) tt = Some ((ft fv))" |
"typeOfExp (lookup a) Nil = None" |
"typeOfExp (lookup a) ((a2, t)#tt) = 
  (if a = a2
  then Some t
  else (typeOfExp (lookup a) tt))"

primrec tcheckPred :: "('id, 'val) Pred \<Rightarrow> ('id, 'ftype) TType \<Rightarrow> bool"
where
"tcheckPred ptrue tt = True" |
"tcheckPred (And p1 p2) tt = ((tcheckPred p1 tt) \<and> (tcheckPred p2 tt))" |
"tcheckPred (Not p) tt = (tcheckPred p tt)" |
"tcheckPred (Eq e1 e2) tt = 
  (let t1 = typeOfExp e1 tt in
    (let t2 = typeOfExp e2 tt in
      (case t1 of 
        None \<Rightarrow> False |
        Some ft1 \<Rightarrow> (case t2 of
          None \<Rightarrow> False |
          Some ft2 \<Rightarrow> (ft1 = ft2)))))" |
"tcheckPred (Gt e1 e2) tt = 
  (let t1 = typeOfExp e1 tt in
    (let t2 = typeOfExp e2 tt in
      (case t1 of 
        None \<Rightarrow> False |
        Some ft1 \<Rightarrow> (case t2 of
          None \<Rightarrow> False |
          Some ft2 \<Rightarrow> (ft1 = ft2)))))" |
"tcheckPred (Lt e1 e2) tt = 
  (let t1 = typeOfExp e1 tt in
    (let t2 = typeOfExp e2 tt in
      (case t1 of 
        None \<Rightarrow> False |
        Some ft1 \<Rightarrow> (case t2 of
          None \<Rightarrow> False |
          Some ft2 \<Rightarrow> (ft1 = ft2)))))"

text_raw {*\DefineSnippet{TopLevelTypeSystem}{*}
inductive typable :: "('id, ('id, 'ftype) TType) Env \<Rightarrow> 
  ('id, 'val) Query \<Rightarrow> ('id, 'ftype) TType \<Rightarrow> bool" 
("_ \<turnstile> _ : _")
where
Ttvalue : "(welltypedtable TT (table al rt)) \<Longrightarrow> TTC \<turnstile> (tvalue (table al rt)) : TT" |
TSelectFromWhere : "\<lbrakk> al = tn#Nil; lookupEnv tn TTC = Some TT; 
                      tcheckPred p TT; 
                      projectType s TT = Some TTr \<rbrakk> \<Longrightarrow>
                    TTC \<turnstile> (selectFromWhere s al p) : TTr" |
TUnion : "\<lbrakk> TTC \<turnstile> q1 : TT; 
            TTC \<turnstile> q2 : TT \<rbrakk> \<Longrightarrow> 
          TTC \<turnstile> (union q1 q2) : TT" |
TIntersection : "\<lbrakk> TTC \<turnstile> q1 : TT; 
                    TTC \<turnstile> q2 : TT \<rbrakk> \<Longrightarrow> 
          TTC \<turnstile> (intersection q1 q2) : TT" |
TDifference : "\<lbrakk> TTC \<turnstile> q1 : TT; 
                  TTC \<turnstile> q2 : TT \<rbrakk> \<Longrightarrow> 
          TTC \<turnstile> (difference q1 q2) : TT"
text_raw {*}%EndSnippet*}

inductive_cases tvalue_INV:
"TTC \<turnstile> (tvalue t) : TT"

inductive_cases selectFromWhere_INV:
"TTC \<turnstile> (selectFromWhere s al p) : TTr"

inductive_cases union_INV:
"TTC \<turnstile> union q1 q2 : TT"

inductive_cases intersection_INV:
"TTC \<turnstile> intersection q1 q2 : TT"

inductive_cases difference_INV:
"TTC \<turnstile> difference q1 q2 : TT"

end

end