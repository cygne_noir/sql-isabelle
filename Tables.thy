theory Tables
imports Main
begin

text_raw {*\DefineSnippet{BasicDatastructuresTables}{*}
type_synonym 'id AttrL = "'id list"
type_synonym 'val Row = "'val list"
type_synonym 'val RawTable = "('val Row) list"

datatype ('id, 'val) Table = table "'id AttrL" "'val RawTable"


locale Table = 
fixes gt :: "'val \<Rightarrow> 'val \<Rightarrow> bool"
 and lt :: "'val \<Rightarrow> 'val \<Rightarrow> bool"
begin
text_raw {*}%EndSnippet*}

primrec getRaw :: "('id, 'val) Table \<Rightarrow> 'val RawTable"
where
"getRaw (table al rt) = rt"

primrec getAttrL :: "('id, 'val) Table \<Rightarrow> 'id AttrL"
where
"getAttrL (table al rt) = al"

(* projects a raw table on its first column *)
fun projectFirstRaw :: "'val RawTable \<Rightarrow> 'val RawTable"
where
"projectFirstRaw Nil = Nil" |
"projectFirstRaw (Nil#rt) = Nil#(projectFirstRaw rt)" |
"projectFirstRaw ((f#r)#rt) = (f#Nil)#(projectFirstRaw rt)"

fun dropFirstColRaw :: "'val RawTable \<Rightarrow> 'val RawTable"
where
"dropFirstColRaw Nil = Nil" |
"dropFirstColRaw (Nil#rt) = Nil#(dropFirstColRaw rt)" |
"dropFirstColRaw ((f#r)#rt) = r#(dropFirstColRaw rt)"

fun attachColToFrontRaw :: "'val RawTable \<Rightarrow> 'val RawTable \<Rightarrow> 'val RawTable"
where
"attachColToFrontRaw Nil Nil = Nil" |
"attachColToFrontRaw ((f#Nil)#rt1) (r#rt2) = 
  ((f#r)#(attachColToFrontRaw rt1 rt2))" |
"attachColToFrontRaw rt1 rt2 = Nil#Nil" 
(* ERROR CASE! idea: return a term which will cause the resulting table to be not welltyped... *)

text_raw {*\DefineSnippet{rawUnion}{*}
fun rawUnion :: "'val RawTable \<Rightarrow> 'val RawTable \<Rightarrow> 'val RawTable"
where
"rawUnion Nil rt2 = rt2" |
"rawUnion rt1 Nil = rt1" |
"rawUnion (r1#rt1) rt2 = 
  (let urt1rt2 = rawUnion rt1 rt2 in
    (if (r1 \<in> (set rt2)) 
      then urt1rt2
      else (r1#urt1rt2)))" 
text_raw {*}%EndSnippet*}

fun rawIntersection :: "'val RawTable \<Rightarrow> 'val RawTable \<Rightarrow> 'val RawTable"
where
"rawIntersection Nil rt2 = Nil" |
"rawIntersection rt1 Nil = Nil" |
"rawIntersection (r1#rt1) rt2 = 
  (let irt1rt2 = rawIntersection rt1 rt2 in
    (if (r1 \<in> (set rt2)) 
      then (r1#irt1rt2)
      else irt1rt2))" 

fun rawDifference :: "'val RawTable \<Rightarrow> 'val RawTable \<Rightarrow> 'val RawTable"
where
"rawDifference Nil rt2 = Nil" |
"rawDifference rt1 Nil = rt1" |
"rawDifference (r1#rt1) rt2 = 
  (let drt1rt2 = rawDifference rt1 rt2 in
    (if (r1 \<in> (set rt2)) 
      then drt1rt2
      else (r1#drt1rt2)))" 

text_raw {*\DefineSnippet{findCol}{*}
fun findCol :: "'id \<Rightarrow> 'id AttrL \<Rightarrow> 'val RawTable \<Rightarrow> ('val RawTable) option"
where
"findCol n Nil rt = None" |
"findCol n (a#al) rt = (if (n = a) 
                      then (Some (projectFirstRaw rt))
                      else (findCol n al (dropFirstColRaw rt)))"
text_raw {*}%EndSnippet*}

primrec projectEmptyCol :: "'val RawTable \<Rightarrow> 'val RawTable"
where
"projectEmptyCol Nil = Nil" |
"projectEmptyCol (r#rt) = Nil#(projectEmptyCol rt)"

text_raw {*\DefineSnippet{projectCols}{*}
primrec projectCols :: "'id AttrL \<Rightarrow> 'id AttrL \<Rightarrow> 'val RawTable \<Rightarrow> ('val RawTable) option"
where
"projectCols Nil al rt = Some (projectEmptyCol rt)" |
"projectCols (a#alr) al rt = 
  (let col = (findCol a al rt) in
    (let rest = (projectCols alr al rt) in
     (case col of
      None \<Rightarrow> None |
      Some rt1 \<Rightarrow> (case rest of
          None \<Rightarrow> None |
          Some rt2 \<Rightarrow> Some (attachColToFrontRaw rt1 rt2)))))"
text_raw {*}%EndSnippet*}
end
end